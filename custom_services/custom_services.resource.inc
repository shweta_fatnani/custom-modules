<?php

/**
 * Callback to check unique username.
 */
function custom_services_unique_user($username) {
  $account = user_load_by_name($username);
    if ($account) {
      return $account;
    }
    else {
      return $username . " does not exists";
    }
}

/**
 * Callback to save preferences.
 */
function custom_services_save_preferences($account) {
  global $user;
  if($user->uid != $account['uid']) {
    return 'User is not logged in. Access denied for Annonymous user.';
  }
  else {
    $account = _services_arg_value($account, 'account');
    $valid_user = user_load($account['uid']);
    if($valid_user) {
      $result = db_select('node', 'n')
      ->fields('n',array('nid'))
      ->condition('uid', $account['uid'],'=')
      ->condition('type', 'user_preference', '=')
      ->execute()
      ->fetchAssoc();
    if(!$result) {
      $node = new stdClass(); 
      $node->type = "user_preference";
      node_object_prepare($node);
      
      $node->title    = "User Preference";
      $node->language = 'und'; 
      
      $node->uid = $account['uid'];
      $delta = 0;
      foreach($account['event_category'] as $events) {
        $tid = taxonomy_get_term_by_name($events, 'event_categories');
        if (isset($tid)) {
            $node->field_event_category[$node->language][$delta]['tid'] = key($tid);
        }
        $delta++;
      }
      $delta = 0;
      foreach($account['news_category'] as $news) {
        $tid = taxonomy_get_term_by_name($news, 'popular_tags');
        if (isset($tid)) {
            $node->field_news_category[$node->language][$delta]['tid'] = key($tid);
        }
        $delta++;
      }
      
      $delta = 0;
      foreach($account['announcement_category'] as $announcement) {
        $tid = taxonomy_get_term_by_name($announcement, 'announcement_category');
        if (isset($tid)) {
            $node->field_announcements_category[$node->language][$delta]['tid'] = key($tid);
        }
        $delta++;
      }
      
      $delta = 0;
      foreach($account['delivery_option'] as $social) {
        $tid = taxonomy_get_term_by_name($social, 'social');
        if (isset($tid)) {
            $node->field_social_share[$node->language][$delta]['tid'] = key($tid);
        }
        $delta++;
      }
      
      if($node = node_submit($node)) {
          node_save($node);
          $message = "Node with nid " . $node->nid . " saved!\n";
      }
    }
    else {
      $node = node_load($result['nid']);
     
      if(isset($account['event_category'])) {
        unset($node->field_event_category['und']);
         $delta = 0;
        foreach($account['event_category'] as $events) {
          $tid = taxonomy_get_term_by_name($events, 'event_categories');
          if (isset($tid)) {
              $node->field_event_category['und'][$delta]['tid'] = key($tid);
          }
          $delta++;
        }
      }
      if(isset($account['news_category'])) {
        unset($node->field_news_category['und']);
        $delta = 0;
        foreach($account['news_category'] as $news) {
          $tid = taxonomy_get_term_by_name($news, 'popular_tags');
          if (isset($tid)) {
              $node->field_news_category['und'][$delta]['tid'] = key($tid);
          }
          $delta++;
        }
      }
      if(isset($account['announcement_category'])) {
        unset($node->field_announcements_category['und']);
        $delta = 0;
        foreach($account['announcement_category'] as $announcement) {
          $tid = taxonomy_get_term_by_name($announcement, 'announcement_category');
          if (isset($tid)) {
              $node->field_announcements_category['und'][$delta]['tid'] = key($tid);
          }
          $delta++;
        }
      }
      if(isset($account['delivery_option'])) {
        unset($node->field_social_share['und']);
        $delta = 0;
        foreach($account['delivery_option'] as $social) {
          $tid = taxonomy_get_term_by_name($social, 'social');
          if (isset($tid)) {
              $node->field_social_share['und'][$delta]['tid'] = key($tid);
          }
          $delta++;
        }
      }  
      
      node_save($node);
      $message = "User Preferences Updated";
    }
    $field_facebook_token = isset($account['fb_token']) ? array('und' => array('0' => array('value' => $account['fb_token']))) : array();
    $field_twitter_token = isset($account['twitter_token']) ? array('und' => array('0' => array('value' => $account['twitter_token']))) : array();
    $field_twitter_secret = isset($account['twitter_secret']) ? array('und' => array('0' => array('value' => $account['twitter_secret']))) : array();
      
    $edit = array('field_facebook_token' => $field_facebook_token, 'field_twitter_token' => $field_twitter_token, 'field_twitter_secret' => $field_twitter_secret );
       
    user_save($valid_user, $edit);
    return $message;
  }
  else {
      return services_error(t('There is no user with ID @uid.', array('@uid' => $account['uid'])), 406);
    }
  }
}

/**
 * Custom service callback to edit user ubercart address.
 */
function custom_services_save_uc_address($uid, $account) {
  global $user;
  if($user->uid != $account['uid']) {
    return 'User is not logged in. Access denied for Annonymous user.';
  }
  else {
    $account = _services_arg_value($account, 'data');
    $valid_user = user_load($account['uid']);
    if($valid_user) {
      
      $valid_user->name = isset($account['name']) ? $account['name'] : $valid_user->name;
      $valid_user->mail = isset($account['mail']) ? $account['mail'] : $valid_user->mail;
      $valid_user->pass = isset($account['pass']) ? $account['pass'] : $valid_user->pass;
      
      user_save($valid_user);
      $addresses = UcAddressesAddressBook::get($account['uid'])->getAddresses();
      
      if(!$addresses) {
        $address = UcAddressesAddressBook::get($uid)->addAddress();
        foreach($account['address'] as $field => $value) {
          $address->setField($field, $value);  
        }
        $address->save();
      }
      else {
        foreach($addresses as $address) {         // Update all instances of address, since there can be only one address in mobile app.
          foreach($account['address'] as $field => $value) {
            $address->setField($field, $value);
          }
          $address->setAsDefault('shipping');
          $address->setAsDefault('billing');
          $address->save();
        }
      }
      return 'User Details updated successfully';
    }
    else {
      return services_error(t('There is no user with ID @uid.', array('@uid' => $uid)), 406);
    }  
  }  
}

/**
 * Custom Service callback to fetch zones.
 */
function custom_services_retrieve_zones($cid) {
  $result = array();
  $query  = db_select('uc_zones', 'uz');
  $query->fields('uz', array('zone_id', 'zone_name'));
  $query->join('uc_countries', 'uc', 'uc.country_id = uz.zone_country_id');
  $query->where('uc.country_id = :cid', array(':cid' => $cid));
  $query->orderBy('uz.zone_name', 'ASC');
  $lstig = $query->execute()->fetchAll();
  if (!empty($lstig)) {
    foreach ($lstig as $value) {
      $result[$value->zone_id] = strtoupper($value->zone_name);
    }
    return $result;
  }
  else {
    return services_error(t('Invalid country id @cid.', array('@cid' => $cid)), 406);
  }
}

/**
 * Custom service callback for auto login link.
 */
function custom_services_register_link($account) {
  $account = _services_arg_value($account, 'account');
  if(isset($account['uid']) && isset($account['destination'])) {
    $auto_login_url = auto_login_url_create($account['uid'], $account['destination'], $absolute = FALSE);
    return $auto_login_url;
  }
  else {
    return 'Invalid arguments';
  }
}