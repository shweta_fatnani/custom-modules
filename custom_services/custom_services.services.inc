<?php

function custom_services_services_resources() {
  $resources = array(
    'check' => array(
    'actions' => array(
        'user' => array(
          'help' => t('Check for unique user.'),
          'file' => array(
            'type' => 'inc',
            'module' => 'custom_services',
            'name' => 'custom_services.resource',
          ),
          'callback' => 'custom_services_unique_user',
          'args' => array(
             array(
               'name' => 'name',
               'type' => 'string',
               'description' => 'Username to check',
               'source' => array('param' => 'name'),
               'optional' => FALSE,
             ),
           ),
          'access callback' => '_custom_services_access',
          'access arguments' => array('check_user'),
          'access arguments append' => TRUE,
        ),
      ),
    ),
    'preferences' => array(
      'actions' => array(
        'save' => array(
          'help' => t('Save preferences'),
          'file' => array(
            'type' => 'inc',
            'module' => 'custom_services',
            'name' => 'custom_services.resource',
          ),
          'callback' => 'custom_services_save_preferences',
          'args' => array(
            array(
              'name' => 'account',
              'type' => 'array',
              'description' => 'User Details',
              'source' => 'data',
              'optional' => FALSE,
            ),
          ),
          'access callback' => '_custom_services_access',
          'access arguments' => array('save_preference'),
          'access arguments append' => FALSE,
        ),
      ), 
    ),
    'uc_address' => array(
      'operations' => array(
        'update' => array(
          'help' => t('Update address of user'),
          'file' => array(
            'type' => 'inc',
            'module' => 'custom_services',
            'name' => 'custom_services.resource',
          ),
          'callback' => 'custom_services_save_uc_address',
          'args' => array(
            array(
              'name' => 'uid',
              'type' => 'int',
              'description' => 'Unique identifier for this user',
              'source' => array('path' => 0),
              'optional' => FALSE,
            ),
            array(
              'name' => 'account',
              'type' => 'array',
              'description' => 'User address Details',
              'source' => 'data',
              'optional' => FALSE,
            ),
          ),
          'access callback' => '_custom_services_access',
          'access arguments' => array('uc_address_update'),
          'access arguments append' => FALSE,
        ),
      ), 
    ),
    'zones' => array(
      'operations' => array(
        'retrieve' => array(
          'help' => t('Retrieve Zones'),
          'file' => array(
            'type' => 'inc',
            'module' => 'custom_services',
            'name' => 'custom_services.resource',
          ),
          'callback' => 'custom_services_retrieve_zones',
          'args' => array(
            array(
              'name' => 'cid',
              'type' => 'int',
              'description' => 'Unique identifier for country',
              'source' => array('path' => 0),
              'optional' => FALSE,
            ),
          ),
          'access callback' => '_custom_services_access',
          'access arguments' => array('retrieve_zones'),
          'access arguments append' => FALSE,
        ),
      ), 
    ),
    'autologin' => array(
    'actions' => array(
        'url' => array(
          'help' => t('Provide Login link'),
          'file' => array(
            'type' => 'inc',
            'module' => 'custom_services',
            'name' => 'custom_services.resource',
          ),
          'callback' => 'custom_services_register_link',
          'args' => array(
            array(
              'name' => 'account',
              'type' => 'array',
              'description' => 'User Details',
              'source' => 'data',
              'optional' => FALSE,
            ),
           ),
          'access callback' => '_custom_services_access',
          'access arguments' => array('check_user'),
          'access arguments append' => TRUE,
        ),
      ),
    ),
  );
  return $resources;
}

/** * Access callback */
function _custom_services_access($op) {
  
  global $user;
  switch($op) {
    case 'check_user':
    case 'retrieve_zones':
      return TRUE;
      break;
    case 'save_preference':
    case 'uc_address_update':
      return TRUE;     //($user->uid != 0 && user_access('access content'));
      break;
  }
  return TRUE;
}