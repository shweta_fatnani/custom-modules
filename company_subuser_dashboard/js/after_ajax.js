(function($, Drupal)
{
	Drupal.ajax.prototype.commands.afterRedirectAjaxCallback = function(ajax, response, status)
	{
		$('.action-button button').remove();
		var content = '<div class="alert alert-block alert-success"><a class="close" data-dismiss="alert" href="#">�</a><h4 class="element-invisible">Status message</h4><ul><li>A message has been sent to Company manager.</li><li>Video - ' + response.status + ' has been rejected.</li></ul></div>';
		$('h1').after(content);
		$.get('remove_drupalsetmessage');
	};
}(jQuery, Drupal));