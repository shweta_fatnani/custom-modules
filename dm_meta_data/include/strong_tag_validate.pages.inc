<?php

/**
 * @file
 * Strong tag valdate page.
 */

/**
 * Strong tag valdate form.
 */
function strong_tag_validate_form($form, &$form_state) {
  global $user;
  if (is_numeric(arg(1))) {

    $form = array();
    $form['#tree'] = TRUE;

    $form['#attached']['css'][]  = drupal_get_path('module', 'company_subuser_dashboard') . '/tags_input/jquery.tagsinput.css';
    $form['#attached']['js'][]   = drupal_get_path('module', 'company_subuser_dashboard') . '/tags_input/jquery.tagsinput.js';
    $form['#attached']['js'][]   = drupal_get_path('module', 'company_subuser_dashboard') . '/tags_input/jquery-ui.min.js';
    $form['#attached']['css'][]  = drupal_get_path('module', 'company_subuser_dashboard') . '/tags_input/jquery-ui.css';
    $form['#attached']['js'][]   = drupal_get_path('module', 'company_subuser_dashboard') . '/freebase_suggest/suggest_implement.js';

    $nid = arg(1);
    $node_data = node_load($nid);
    $parent_user = node_load($node_data->field_parent_node_reference['und'][0]['target_id']);
    $companymanager = $parent_user->field_job_assigned_company['und'][0]['target_id'];
    $node_lang = isset($parent_user->field_cat_language[LANGUAGE_NONE][0]['tid']) ? $parent_user->field_cat_language[LANGUAGE_NONE][0]['tid'] : '';
    if (isset($node_lang)) {
      $_SESSION['vdata']['lang'] = 'en';
      unset($_SESSION['vdata']);
      $video_lang = taxonomy_term_load($node_lang);
      $_SESSION['vdata']['lang'] = ($video_lang) ? $video_lang->field_language_code[LANGUAGE_NONE][0]['value'] : 'en';
    }

    if (!empty($user->roles[4])) {
      if(!empty($node_data->field_strong_tags_added[LANGUAGE_NONE])) {
        foreach ($node_data->field_strong_tags_added[LANGUAGE_NONE] as $node_data_value) {
          $tags_array[] = $node_data_value['value'];
        }
        if ($tags_array) {
          $tags = rtrim(implode(', ', $tags_array), ', ');
        }  
      }
      
      $subuser_loaded = user_load($node_data->field_company_subuser_refernce['und'][0]['target_id']);

      $form['video_nid'] = array('#type' => 'hidden', '#value' => $node_data->nid);

      $form['Send_comment'] = array(
        '#type' => 'link',
        '#title' => 'Send Message',
        '#href' => 'dmadmin_comment/nojs/messages/new/' . $companymanager . '/' . $node_data->field_video_id['und'][0]['value'] . '/' . $node_data->title . '/' . $subuser_loaded->field_company_name['und'][0]['value'] ,
        '#attributes' => array('class' => array('btn btn-default ctools-use-modal')),
      );
      $form['comment-on-reject'] = array(
        '#type' => 'link',
        '#title' => 'Send a Comment',
        '#href' => 'dmadmin_comment/nojs/messages/new/' . $companymanager . '/' . $node_data->field_video_id['und'][0]['value'] . '/' . $node_data->title . '/' . $subuser_loaded->field_company_name['und'][0]['value'] . '%' . $node_data->nid ,
        '#attributes' => array('class' => array('comment-on-reject btn btn-default ctools-use-modal')),
      );

      $form['video_detail'] = array(
        '#title' => t('Edit Strong Tag'),
        '#type' => 'item',
        '#markup' => '<div class="upper-header"> Video ID: ' . $node_data->field_video_id['und'][0]['value'] . '</div>'
        . '<div class="lower-header"> <div class="v-date">Date</div>  <div class="v-jobdone">Job Done by</div>'
        . ' <div class="v-videotitle">Original Title</div> <div class="v-videodescription">Original Description</div>  </div>'
        . '<div class="height-cutter-table-result"><div class="table-result"> <div class="v-date">' . date("m/d/Y", $node_data->created) . '</div>'
        . '<div class="v-jobdone">' . $subuser_loaded->field_company_name['und'][0]['value'] . '</div>'
        . '<div class="v-videotitle">' . $node_data->title . '</div>'
        . '<div class="v-videodescription">' . render($node_data->body['und'][0]['value']) . '</div> </div></div>',
      );

      $form["strong_tags_added"] = array(
        '#type' => 'textarea',
        '#title' => 'Original Strongs Tags',
        '#rows' => 4,
        '#disabled' => TRUE,
      );

      $form["strong_tags_tobe_add"] = array(
        '#type' => 'textarea',
        '#title' => 'Strong Tags Added',
        '#rows' => 4,
        '#default_value' => !empty($tags) ? $tags : '',
        '#attributes' => array('class' => array('freebase-suggestion')),
      );

      $form['reject'] = array(
        '#type' => 'button',
        '#value' => 'Reject',
        '#prefix' => '<div class="action-button">',
      );

      $form['cancel'] = array(
        '#type' => 'submit',
        '#value' => 'Cancel',
      );

      $form['validate'] = array(
        '#type' => 'submit',
        '#value' => 'Validate',
        '#sufix' => '</div>',
      );
      return $form;
    }
    else {
      drupal_access_denied();
    }
  }
  else {
    drupal_set_message('Video id not found.', 'error');
  }
}

/**
 * Form validate().
 */
function strong_tag_validate_form_validate($form, &$form_state) {
  switch ($form_state['input']['op']) {
    case 'Reject':

      break;
    case 'Validate':
      $tags = trim($form_state['values']['strong_tags_tobe_add']);
      if (empty($tags)) {
        form_set_error('strong_tags_tobe_add', 'At least one strong tag is required.');
      }
      elseif (!empty($tags)) {
        $tags_array = explode(",", $tags);
        $counted_tags = count($tags_array);
        if ($counted_tags > 5) {
          form_set_error('strong_tags_tobe_add', t('You can not add more than 5 tags.'));
        }
      }
      break;
  }
}

/**
 * Form submit().
 */
function strong_tag_validate_form_submit($form, &$form_state) {
  global $user;
  $load_user = user_load($user->uid);
  $fullname = $load_user->field_first_name[LANGUAGE_NONE][0]['value'] . ' ' . $load_user->field_last_name[LANGUAGE_NONE][0]['value'];
  switch ($form_state['input']['op']) {
    case 'Reject':
      $node = node_load($form_state['values']['video_nid']);
      // For rejected.
      $node->field_vm_status['und'][0]['tid'] = 16;
      $node->field_date_of_job_rejected['und'][0]['value'] = date("Y-m-d h:m:s");
      node_save($node);
      drupal_set_message($node->field_video_id['und'][0]['value'] . ' video rejected.', 'status');
      drupal_goto('jobs-status');
      break;

    case 'Cancel';
      drupal_goto('jobs-status');
      break;

    case 'Validate';
      $dmobject = new DailymotionOwnMethod();
      $node = node_load($form_state['values']['video_nid']);
      if (!empty($form_state['values']['strong_tags_tobe_add'])) {
        $tags = explode(",", $form_state['values']['strong_tags_tobe_add']);
        if (!empty($tags)) {
          foreach ($tags as $keys => $value) {
            $tags_array = explode(":", $value);
            $channel = $node->field_video_channel[LANGUAGE_NONE][0]['value'];
            $data = $dmobject->addStrongtagsVideoOnDailymotion($node->field_video_id[LANGUAGE_NONE][0]['value'], 'freebase', $tags_array[2], $tags_array[1], $channel);
            $data = json_decode($data);
            if ($data->status == 'ok') {
              $message['status'] = 'Strong tag ' . $data->name . ', mid : ' . $tags_array[1] . 'Submitted to video ' . $node->field_video_id[LANGUAGE_NONE][0]['value'];            
            }
            else {
              $message['error'] = $data->message;
            }
            $node->field_strong_tags_added[LANGUAGE_NONE][$keys]['value'] = $value;
          }
          if (isset($message) && !array_key_exists('error', $message)) {
            $node->field_vm_status[LANGUAGE_NONE][0]['tid'] = 15;
            $node->field_date_of_job_validated[LANGUAGE_NONE][0]['value'] = date("Y-m-d h:m:s");
            $node->field_validated_rejected[LANGUAGE_NONE][0]['value'] = $fullname;
            node_save($node);
            drupal_set_message($node->field_video_id[LANGUAGE_NONE][0]['value'] . ' video validated.', 'status');
            drupal_goto('jobs-status');
          }
          else {
            foreach ($message as $key => $value) {
              drupal_set_message($value, $key);
            }
          }
        }
      }
      
      break;
  }
}
