<?php

/**
 * @file
 * Strong tag valdate page.
 */

/**
 * Strong tag valdate form.
 */
function video_translate_validate_form($form, &$form_state) {
  global $user;
  if (is_numeric(arg(1))) {

    $form = array();
    $form['#tree'] = TRUE;

    $nid = arg(1);
    $node_data = node_load($nid);
    $parent_user = node_load($node_data->field_parent_node_reference['und'][0]['target_id']);
    $companymanager = $parent_user->field_job_assigned_company['und'][0]['target_id'];

    if (!empty($user->roles[4])) {
      $subuser_loaded = user_load($node_data->field_company_subuser_refernce['und'][0]['target_id']);

      $form['video_nid'] = array('#type' => 'hidden', '#value' => $node_data->nid);

      $form['Send_comment'] = array(
        '#type' => 'link',
        '#title' => 'Send Message',
        '#href' => 'dmadmin_comment/nojs/messages/new/' . $companymanager . '/' . $node_data->field_video_id['und'][0]['value'] . '/' . $node_data->title . '/' . $subuser_loaded->field_company_name['und'][0]['value'] ,
        '#attributes' => array('class' => array('btn btn-default ctools-use-modal')),
      );
      $form['comment-on-reject'] = array(
        '#type' => 'link',
        '#title' => 'Send a Comment',
        '#href' => 'dmadmin_comment/nojs/messages/new/' . $companymanager . '/' . $node_data->field_video_id['und'][0]['value'] . '/' . $node_data->title . '/' . $subuser_loaded->field_company_name['und'][0]['value'] . '%' . $node_data->nid ,
        '#attributes' => array('class' => array('comment-on-reject btn btn-default ctools-use-modal')),
      );

      $form['video_detail'] = array(
        '#title' => t('Edit Translation'),
        '#type' => 'item',
        '#markup' => '<div class="upper-header"> Video ID: ' . $node_data->field_video_id['und'][0]['value'] . '</div>'
        . '<div class="lower-header"> <div class="v-date">Date</div>  <div class="v-jobdone">Job done by</div>'
        . ' <div class="v-videotitle">Original Title</div> <div class="v-videodescription">Original Description</div>  </div>'
        . '<div class="height-cutter-table-result"><div class="table-result"> <div class="v-date">' . date("m/d/Y", $node_data->created) . '</div>'
        . '<div class="v-jobdone">' . $subuser_loaded->field_company_name['und'][0]['value'] . '</div>'
        . '<div class="v-videotitle">' . $node_data->title . '</div>'
        . '<div class="v-videodescription">' . render($node_data->body['und'][0]['value']) . '</div> </div></div>',
      );

      $form['translated_title'] = array(
        '#title' => 'Translated Title',
        '#type' => 'textfield',
        '#default_value' => isset($node_data->field_translated_title[LANGUAGE_NONE][0]['value']) ? $node_data->field_translated_title[LANGUAGE_NONE][0]['value'] : '',
      );

      $form['translated_desc'] = array(
        '#title' => 'Translated Description',
        '#type' => 'textarea',
        '#default_value' => isset($node_data->field_translated_description[LANGUAGE_NONE][0]['value']) ? $node_data->field_translated_description[LANGUAGE_NONE][0]['value'] : '',
      );

      $form['reject'] = array(
        '#type' => 'button',
        '#value' => 'Reject',
        '#prefix' => '<div class="action-button">',
      );

      $form['cancel'] = array(
        '#type' => 'submit',
        '#value' => 'Cancel',
      );

      $form['validate'] = array(
        '#type' => 'submit',
        '#value' => 'Validate',
        '#sufix' => '</div>',
      );
      return $form;
    }
    else {
      drupal_access_denied();
    }
  }
  else {
    drupal_set_message('Video id not found.', 'error');
  }
}

/**
 * Form validate().
 */
function video_translate_validate_form_validate($form, &$form_state) {
  switch ($form_state['input']['op']) {
    case 'Reject':

      break;

    case 'Validate':
      if (empty($form_state['values']['translated_title'])) {
        form_set_error('translated_title', 'Translated title should not be blank.');
      }

      if (empty($form_state['values']['translated_desc'])) {
        form_set_error('translated_desc', 'Translated description should not be blank.');
      }
      break;
  }
}

/**
 * Form submit().
 */
function video_translate_validate_form_submit($form, &$form_state) {
  global $user;
  $load_user = user_load($user->uid);
  $fullname = $load_user->field_first_name[LANGUAGE_NONE][0]['value'] . ' ' . $load_user->field_last_name[LANGUAGE_NONE][0]['value'];
  switch ($form_state['input']['op']) {
    case 'Reject':
      $node = node_load($form_state['values']['video_nid']);
      // For rejected.
      $node->field_vm_status['und'][0]['tid'] = 16;
      $node->field_date_of_job_rejected['und'][0]['value'] = date("Y-m-d h:m:s");
      node_save($node);
      drupal_set_message($node->field_video_id['und'][0]['value'] . ' video rejected.', 'status');
      drupal_goto('translate-jobs-status');
      break;

    case 'Cancel':
      drupal_goto('translate-jobs-status');
      break;

    case 'Validate':
      $dmobject = new DailymotionOwnMethod();
      $node = node_load($form_state['values']['video_nid']);
      $video_data->field_translated_title[LANGUAGE_NONE][0]['value'] = $form_state['values']['translated_title'];
      $video_data->field_translated_description[LANGUAGE_NONE][0]['value'] = $form_state['values']['translated_desc'];

      $from_channel = $node->field_video_channel[LANGUAGE_NONE][0]['value'];
      $dest_channel = explode(":", $node->field_video_destination_channel[LANGUAGE_NONE][0]['value']);
      $to_channel = $dest_channel[0];
      $url = $dmobject->dailymotionlink . 'video/' . $node->field_video_id[LANGUAGE_NONE][0]['value'];
      $description = $form_state['values']['translated_desc'];
      $title = $form_state['values']['translated_title'];
      $language = getVideoLanguage($node->field_parent_node_reference[LANGUAGE_NONE][0]['target_id']);
      $data = $dmobject->VideoDuplicateOnDailymotion($from_channel, $to_channel, $url, $description, $title, $language);
      $response = json_decode($data);
      if ($response->status == 'ok') {
        $node->field_vm_status[LANGUAGE_NONE][0]['tid'] = 15;
        $node->field_date_of_job_validated[LANGUAGE_NONE][0]['value'] = date("Y-m-d h:m:s");
        $node->field_validated_rejected[LANGUAGE_NONE][0]['value'] = $fullname;
        node_save($node);
        drupal_set_message($node->field_video_id[LANGUAGE_NONE][0]['value'] . ' video validated.', 'status');
        drupal_goto('translate-jobs-status');
      }
      else {
        form_set_error($response->message);
      }
      break;
  }
}
