<?php

/**
 * @file
 * DM meta submit job form inc file.
 */

/**
 * Theme callback for menu.
 */
function submit_job_theme_callback() {
  return theme('assign_themplate_to_submit_job_menu', array(
    'search_data' => @$_SESSION['search_data']
  ));
}

/**
 * Submit job form.
 *
 * Code for get playlist written in dm_meta_data.module
 */
function submit_job_config_form($form, &$form_state) {
  $form = $playlists = array();
  $get_user_id = '';
  $job_type = taxonomy_options_array('job_type');
  if (!empty($form_state['values']['searchable']['user_listing'])) {
    $get_user_id = explode(":", $form_state['values']['searchable']['user_listing']);
    $Childusers = get_dailymotion_childusers($get_user_id[1]);
    $playlists = get_dailymotion_playlists($get_user_id[1]);
  }
  elseif (!empty($_SESSION['search_data']['user_listing'])) {
    $get_user_id = explode(":", $_SESSION['search_data']['user_listing']);
    $Childusers = @get_dailymotion_childusers($get_user_id[1]);
    $playlists = @get_dailymotion_playlists($get_user_id[1]);
  }

  $form['#tree'] = TRUE;
  $form['searchable']['#prefix'] = '<div id="form-wrapper">';
  $form['searchable']['#suffix'] = '</div>';

  $form['searchable']['job_type'] = array(
    '#type' => 'select',
    '#title' => t('Job Type'),
    '#options' => $job_type,
    '#prefix' => '<div class="container-inline1">',
    '#suffix' => '</div>',
    '#default_value' => !empty($_SESSION['search_data']['job_type']) ? array(
      $_SESSION['search_data']['job_type']) : NULL,
  );

  $form['searchable']['video_search_type'] = array(
    '#type' => 'radios',
    '#title' => t('Job Submission Type'),
    '#options' => array('reg' => 'Regular', 'url' => 'URLs'),
    '#default_value' => !empty($_SESSION['search_data']['video_search_type']) ? $_SESSION['search_data']['video_search_type'] : 'url',
  );

  $form['searchable']['language'] = array(
    '#type' => 'select',
    '#title' => t('Original Language'),
    '#options' => get_languages(),
    '#prefix' => '<div class="container-inline1">',
    '#suffix' => '</div>',
    '#required' => TRUE,
    '#default_value' => !empty($_SESSION['search_data']['language']) ? array(
      $_SESSION['search_data']['language']) : NULL,
  );

  $form['searchable']['dest_language'] = array(
    '#type' => 'select',
    '#title' => t('Destination Language'),
    '#options' => get_languages_with_tid(),
    '#required' => TRUE,
    '#default_value' => !empty($_SESSION['search_data']['dest_language']) ? array(
      $_SESSION['search_data']['dest_language']) : array(50 => 50),
    '#states' => array(
      'visible' => array(
        'select[name="searchable[job_type]"]' => array('value' => 2),
      ),
    ),
  );

  $form['searchable']['user_listing'] = array(
    '#title' => t('Original Channel'),
    '#type' => 'textfield',
    '#maxlength' => 60,
    '#size' => 30,
    '#autocomplete_path' => 'dmusers/autocomplete',
    '#default_value' => !empty($_SESSION['search_data']['user_listing']) ? $_SESSION['search_data']['user_listing'] : NULL,
    '#ajax' => array(
      'callback' => 'destination_channel_ajax_callback',
      'wrapper' => 'autocomplete-for-dest-channel-ajax-replace',
    ),
  );

  $form['searchable']['generate_palylist'] = array(
    '#type' => 'submit',
    '#value' => t('Fetch Playlists'),
    '#states' => array(
      'visible' => array(
        'input[name="searchable[video_search_type]"]' => array('value' => 'reg'),
      ),
    ),
    '#submit' => array(
      'generate_palylist_submit_callback'
    ),
    '#ajax' => array(
      'callback' => 'generate_palylist_callback',
      'wrapper' => 'dailymotion-playlist',
    ),
  );

  $form['searchable']['playlists'] = array(
    '#type' => 'select',
    '#title' => t('Playlists'),
    '#options' => !empty($playlists) ? $playlists : array(0 => '-- No playlist --'),
    '#states' => array(
      'visible' => array(
        'input[name="searchable[video_search_type]"]' => array('value' => 'reg'),
      ),
    ),
    '#prefix' => '<div id="dailymotion-playlist">',
    '#suffix' => '</div>',
    '#default_value' => !empty($_SESSION['search_data']['playlists']) ? array(
      $_SESSION['search_data']['playlists']) : NULL,
  );

  $form['searchable']['dest_user_listing'] = array(
    '#title' => t('Destination Channel'),
    '#type' => 'select',
    '#prefix' => '<div id="autocomplete-for-dest-channel-ajax-replace">',
    '#suffix' => '</div>',
    '#options' => !empty($Childusers) ? $Childusers : array(0 => '-- No Child --'),
    '#default_value' => !empty($_SESSION['search_data']['dest_user_listing']) ? $_SESSION['search_data']['dest_user_listing'] : NULL,
    '#states' => array(
      'visible' => array(
        ':input[name="searchable[job_type]"]' => array('value' => 2),
      ),
    ),
  );

  $form['searchable']['paste_urls'] = array(
    '#type' => 'textarea',
    '#title' => t('Paste URLs Here'),
    '#default_value' => !empty($_SESSION['search_data']['paste_urls']) ? $_SESSION['search_data']['paste_urls'] : '',
    '#states' => array(
      'visible' => array(
        'input[name="searchable[video_search_type]"]' => array('value' => 'url'),
      ),
    ),
  );

  $form['searchable']['tags'] = array(
    '#type' => 'textfield',
    '#title' => t('Keywords'),
    '#prefix' => '<div class="container-inline1">',
    '#suffix' => '</div>',
    '#default_value' => !empty($_SESSION['search_data']['tags']) ? ($_SESSION['search_data']['tags']) : array(),
    '#size' => 30,
    '#states' => array(
      'visible' => array(
        'input[name="searchable[video_search_type]"]' => array('value' => 'reg'),
      ),
    ),
  );

  $form['searchable']['page'] = array(
    '#type' => 'hidden',
    '#value' => 1
  );

  $form['searchable']['limit'] = array(
    '#type' => 'hidden',
    '#value' => 20,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );

  $form['clear'] = array(
    '#type' => 'submit',
    '#value' => t('Clear'),
  );
  return ($form);
}

/**
 * Validate submit job form.
 */
function generate_palylist_callback($form, &$form_state) {
  return $form['searchable']['playlists'];
}

/**
 * Validate submit job form.
 */
function generate_palylist_submit_callback($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * Ajax callback for autocomplete of dest channel.
 */
function destination_channel_ajax_callback($form, &$form_state) {
  if($form_state['values']['searchable']['job_type'] == 2)
    return $form['searchable']['dest_user_listing'];
}

/**
 * Validate submit job form.
 */
function submit_job_config_form_validate($form, &$form_state) {
  global $user;
  if (isset($form_state['input']['op']) && ($form_state['input']['op'] == 'Search')) {
    db_delete('dm_temp_submit_job_from_url')
        ->condition('uid', $user->uid, '=')
        ->execute();
    if ($form_state['values']['searchable']['job_type'] == 1)  {
      $org_ch = explode(":", trim($form_state['values']['searchable']['user_listing']));
      if(empty($org_ch[1])) {
        form_set_error('', t('Original Channel is required.'));
      }
    }
    if ($form_state['values']['searchable']['job_type'] == 2) {
      $des_ch = explode(":", trim($form_state['values']['searchable']['dest_user_listing']));
      $org_ch = explode(":", trim($form_state['values']['searchable']['user_listing']));
      if(!empty($org_ch[1]) && !empty($des_ch[1])) {
        $dmobject = new DailymotionOwnMethod();
        $result = $dmobject->checkParentUserofDailymotion($des_ch[1], $org_ch[1]);
        if (empty($result)) {
           form_set_error('dest_user_listing', 'Destination Channel is not the child of original Channel.');
        }
      }
      else {
        form_set_error('', t('Destination Channel is not the child of Original Channel.'));
      }      
    }
    if ($form_state['values']['searchable']['video_search_type'] == 'url') {
      if (!empty($form_state['values']['searchable']['paste_urls'])) {
        $dm_user = chunk_data_for_video_playlist_user($form_state['values']['searchable']);
        if(isset($dm_user['error'])) {
          foreach ($dm_user['error'] as $key => $value) {
            $error_text = $value['error'] . " for URL :-  " . $value['url'];
            drupal_set_message(t($error_text), 'warning');
          }
        }
        if(empty($dm_user['user_name'])) {
          form_set_error('', t('User name did not match for any url.'));
        }
        if (empty($dm_user['user_name']) && empty($dm_user['playlist']) && empty($dm_user['videos'])) {
          form_set_error('', t('Invaild URLs.'));
        }
      }
      else {
        form_set_error('', t('Paste URLs field is empty.'));
      } 
    }
  }
}

/**
 * Submit handler submit job form.
 */
function submit_job_config_form_submit($form, &$form_state) {
  global $user;
  if ($form_state['input']['op'] == 'Clear') {
    $_SESSION['search_data'] = '';
  }
  elseif ($form_state['input']['op'] == 'Search') {
    db_delete('dm_temp_submit_job_from_url')
        ->condition('uid', $user->uid, '=')
        ->execute();
    $_SESSION['search_data'] = '';
    if ($form_state['values']['searchable']['video_search_type'] == 'url') {
      $_SESSION['check'] = 'yes';
      $_SESSION['search_data'] = chunk_data_for_video_playlist_user($form_state['values']['searchable']);
      unset($_SESSION['search_data']['error']);
    }
    else {
      $_SESSION['search_data'] = array_filter($form_state['values']['searchable']);
      $_SESSION['check'] = 'yes';
      if ($form_state['values']['searchable']['playlists']) {
        $playlist_id = $form_state['values']['searchable']['playlists'];
        $_SESSION['search_data']['playlist_name'] = $form_state['complete form']['searchable']['playlists']['#options'][$playlist_id];
      }
    }
    $_SESSION['search_data']['dest_language'] = $form_state['values']['searchable']['dest_language'];
    $_SESSION['search_data']['dest_user_listing'] = $form_state['values']['searchable']['dest_user_listing'];
  }
}

/**
 * Chunk data for video playlist user.
 */
function chunk_data_for_video_playlist_user($data) {
  $result = array();
  $user_count = 0;
  $result['job_type'] = $data['job_type'];
  $result['language'] = $data['language'];
  $result['video_search_type'] = $data['video_search_type'];
  $result['page'] = $data['page'];
  $result['limit'] = $data['limit'];
  $result['paste_urls'] = $data['paste_urls'];
  $result['user_listing'] = $data['user_listing'];
  $org_user = explode(':', $data['user_listing']);
  $user_name = $org_user[0];
  
  $regex_list;
  $url_pattern = '/.*\.?dailymotion\.com\/(.*?)\/(.*?)[_\/].*/';
  preg_match_all($url_pattern, $result['paste_urls'], $regex_list);
  $url_count = count($regex_list[0]);
  $urls = 0;
  $types = 1;
  $ids = 2;
  $i = 0;
  while($i<$url_count) {
    $current_url_data = array(
      'id' => $regex_list[$ids][$i],
      'url' => $regex_list[$urls][$i],
      'type' => $regex_list[$types][$i],
    );
    process_dm_result_array($current_url_data, $user_name, $result);
    ++$i;
  }
  return $result;
}

/**
 * Check username validity
 */
function process_dm_result_array($data, $username, &$result) {
  if($data['type'] == 'user') {
    if($data['id'] == $username) {
      $result['user_name'] = $username;
    } else {
      $result['error'][] = array(
        'error' => "Username ( " . $data['id'] . " ) did not matched",
        'url' => $data['url'],
      );
    }

  } else {
    $index_name = ($data['type'] == 'video') ? 'videos' : 'playlist';
    $result[$index_name][] = $data['id'];
    $dmobject = new DailymotionOwnMethod();
    $owner = $dmobject->getDailyMotionAssociatedUser($data['type'], $data['id']);
    if(empty($owner['owner.username'])) {
      $result['error'][] = array(
        'error' => isset($owner['error']) ? $owner['error'] : '',
        'url' => $data['url'],
        );
    } else {
      if($owner['owner.username'] == $username) {
        $result['user_name'] = $username;
      } else {
          $result['error'][] = array(
            'error' => "Username ( " . $owner['owner.username'] . " ) did not match",
            'url' => $data['url'],
          );
      }
    }
  }
}

/**
 * Assign job to company form.
 */
function assign_job_company_form($form, &$form_state, $search_datas = NULL) {
  $form = array();
  $form['#tree'] = TRUE;
  $form['#after_build'][] = 'assign_company_job_after_build';

  if (empty($form_state['num_names'])) {
    $form_state['num_names'] = 1;
    $form_state['offset'] = 0;
    $form_state['perpage'] = 50;
  }

  if (!empty($form_state['values']['video_listing_fieldset']['perpage'])) {
    $form_state['perpage'] = $form_state['values']['video_listing_fieldset']['perpage'];
  }

  if (!empty($search_datas)) {
    $form_state['search_datas'] = $search_datas;
  }

  if ($search_datas['video_search_type'] == 'reg') {
    if (isset($_SESSION['check']) == 'yes') {
      generate_video_listing($form_state['num_names'], $form_state['search_datas']);
      unset($_SESSION['check']);
    }
    $video_options = get_temp_video_list($form_state['offset'], $form_state['perpage']);
  }

  if ($search_datas['video_search_type'] == 'url') {
    if (isset($_SESSION['check']) == 'yes') {
      generate_video_listing_using_urls($form_state['num_names'], $form_state['search_datas']);
      unset($_SESSION['check']);
    }
    $video_options = get_temp_video_list($form_state['offset'], $form_state['perpage']);
  }

  $form['company_user'] = array(
    '#type' => 'select',
    '#title' => t('Assign the job to'),
    '#options' => get_all_company_name(5),
    '#required' => TRUE,
  );

  $form['comment'] = array(
    '#type' => 'textarea',
    '#title' => t('Comment'),
  );

  $form['user_name'] = array(
    '#type' => 'hidden',
    '#value' => !empty($_SESSION['search_data']['user_name']) ? $_SESSION['search_data']['user_name'] : ' ',
  );

  $form['playlist_name'] = array(
    '#type' => 'hidden',
    '#value' => !empty($_SESSION['search_data']['playlist_name']) ? $_SESSION['search_data']['playlist_name'] : ' ',
  );

  if (!empty($video_options['result'])) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Assign'),
    );
  }

  $form['selected_jobtype'] = array(
    '#type' => 'hidden',
    '#value' => !empty($search_datas['job_type']) ? $search_datas['job_type'] : NULL,
    '#required' => TRUE,
  );

  $forall_pages = get_temp_video_list_for_allpages();
  if (!empty($_SESSION['search_data']['job_type']) && $_SESSION['search_data']['job_type'] == 2) {
    $assigned = get_already_assigned_videos2($_SESSION['search_data']['job_type'], $_SESSION['search_data']['dest_language']);
  }
  else {
    $assigned = get_already_assigned_videos2($_SESSION['search_data']['job_type']);
  }
  $left_videos = array_diff($forall_pages, $assigned);

  $form['for_all_pages'] = array(
    '#type' => 'hidden',
    '#value' => ($forall_pages) ? $forall_pages : array(),
  );

  if (!empty($video_options['result'])) {
    $form['video_listing_fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => '',
      '#prefix' => '<div id="video-listing-wrapper">',
      '#suffix' => '</div>',
    );

    $form['video_listing_fieldset']['radio_click'] = array(
      '#type' => 'checkboxes',
      '#options' => array('Check all'),
      '#prefix' => '<div class="open-select-all">',
      '#suffix' => '</div>',
    );

    $form['video_listing_fieldset']['select_all'] = array(
      '#type' => 'select',
      '#multiple' => TRUE,
      '#options' => array('Only this page', 'All pages', 'Remove all'),
      '#prefix' => '<div class="select_all">',
      '#suffix' => '</div>',
    );

    $form['video_listing_fieldset']['perpage'] = array(
      '#type' => 'select',
      '#options' => array(50 => '50 per page', 100 => '100 per page',
        200 => '200 per page', 500 => '500 per page', 1000 => '1000 per page'),
      '#ajax' => array(
        'callback' => 'ajax_perpage_submit_callback',
        'wrapper' => 'video-listing-wrapper',
      ),
    );
    $form['video_listing_fieldset']['left_videos'] = array(
      '#type' => 'markup',
      '#markup' => '<span class="left-videos-display">Remaining (' . count($left_videos) . ') / Total (' . count($forall_pages) . ')</span>',
    );
  }

  if ($form_state['num_names'] > 1) {
    $form['video_listing_fieldset']['prev_records'] = array(
      '#type' => 'submit',
      '#value' => t('PREV'),
      '#limit_validation_errors' => array(),
      '#submit' => array(
        'ajax_prev_records_submit_callback',
      ),
      '#ajax' => array(
        'callback' => 'assign_job_company_callback',
        'wrapper' => 'video-listing-wrapper',
      ),
    );
  }

  if (!empty($video_options['has_more'])) {
    $form['video_listing_fieldset']['next_records'] = array(
      '#type' => 'submit',
      '#value' => t('NEXT'),
      '#limit_validation_errors' => array(),
      '#submit' => array(
        'ajax_next_records_submit_callback',
      ),
      '#ajax' => array(
        'callback' => 'assign_job_company_callback',
        'wrapper' => 'video-listing-wrapper',
      ),
    );
  }

  if ($form_state['offset'] > 0) {
    $form['video_listing_fieldset']['prev_record'] = array(
      '#type' => 'submit',
      '#value' => t('PREV'),
      '#limit_validation_errors' => array(),
      '#submit' => array(
        'ajax_prev_offset_records_submit_callback',
      ),
      '#ajax' => array(
        'callback' => 'assign_job_company_callback',
        'wrapper' => 'video-listing-wrapper',
      ),
    );
  }

  if (!empty($video_options['count'])) {
    $form['video_listing_fieldset']['next_record'] = array(
      '#type' => 'submit',
      '#value' => t('NEXT'),
      '#limit_validation_errors' => array(),
      '#submit' => array(
        'ajax_offset_records_submit_callback',
      ),
      '#ajax' => array(
        'callback' => 'assign_job_company_callback',
        'wrapper' => 'video-listing-wrapper',
      ),
    );
  }

  if (!empty($video_options['result'])) {
    $form['video_listing_fieldset']['videolistings'] = array(
      '#type' => 'checkboxes',
      '#options' => !empty($video_options['result']) ? $video_options['result'] : array(),
      '#prefix' => '<div class="box-content">',
      '#suffix' => '</div>',
    );
  }
  else {
    $form['markup'] = array(
      '#type' => 'markup',
      '#prefix' => '<div class="no-result-found">',
      '#suffix' => '</div>',
      '#markup' => '<h2>Sorry. No Result Found.</h2>',
    );
  }
  return ($form);
}

/**
 * Assign job company after build form.
 */
function assign_company_job_after_build(&$form, $form_state) {
  $form['#attached']['js'][] = drupal_get_path('module', 'dm_meta_data') . '/js/submit_job.js';
  return $form;
}

/**
 * Get term id by channel machine name.
 *
 * @Param string channel machine name.
 *
 * @Return int  corresponsing channel term id.
 */
function assign_job_company_callback($form, &$form_state) {
  return $form['video_listing_fieldset'];
}

/**
 * Increments the max counter and causes a rebuild.
 */
function ajax_perpage_submit_callback($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  return $form['video_listing_fieldset'];
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * Increments the max counter and causes a rebuild.
 */
function ajax_next_records_submit_callback($form, &$form_state) {
  $form_state['num_names']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the "remove one" button.
 *
 * Decrements the max counter and causes a form rebuild.
 */
function ajax_prev_records_submit_callback($form, &$form_state) {
  if ($form_state['num_names'] > 1) {
    $form_state['num_names']--;
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * Increments the max counter and causes a rebuild.
 */
function ajax_offset_records_submit_callback($form, &$form_state) {
  $form_state['offset'] = $form_state['offset'] + $form_state['perpage'];
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the "remove one" button.
 *
 * Decrements the max counter and causes a form rebuild.
 */
function ajax_prev_offset_records_submit_callback($form, &$form_state) {
  if ($form_state['offset'] > 0) {
    $form_state['offset'] = $form_state['offset'] - $form_state['perpage'];
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Get all video list from temp table.
 */
function get_temp_video_list_for_allpages() {
  global $user;
  $rows = array();

  $result = db_select('dm_temp_submit_job_from_url', 'tvd')
          ->fields('tvd', array('id'))
          ->condition('uid', $user->uid, '=')
          ->execute()->fetchAll();

  foreach ($result as $key => $media) {
    $rows[$media->id] = $media->id;
  }
  return $rows;
}

/**
 * Get all video list.
 */
function get_temp_video_list($offset = 0, $per_page = 100) {
  global $user;
  if ($offset < 0) {
    $offset = 0;
  }
  if ($per_page <= 0) {
    $per_page = 50;
  }
  $rows = array();
  $query = db_select('dm_temp_submit_job_from_url', 'tvd2')
          ->fields('tvd2', array('id'))
          ->condition('uid', $user->uid, '=')
          ->execute()->rowCount();
  if (($offset + $per_page) < $query) {
    $rows['count'] = $query;
  }

  $result = db_select('dm_temp_submit_job_from_url', 'tvd')
          ->fields('tvd', array('id', 'title', 'thumbnail_url'))
          ->condition('uid', $user->uid, '=')
          ->range($offset, $per_page)->execute();

  foreach ($result as $key => $media) {
    $image = !empty($media->thumbnail_url) ? theme('image', array(
          'path' => $media->thumbnail_url,
          'width' => '150px',
          'height' => '150px'
        )) : 'No Image Found';
    $title = '<div class="sd_video_title small title">' . truncate_utf8($media->title, 15, TRUE) . '</div>';
    $rows['result'][$media->id] = t('!title !thumbnail_image', array(
      '!title' => $title,
      '!thumbnail_image' => $image
    ));
  }
  return $rows;
}

/**
 * Validate assign job to company form.
 */
function assign_job_company_form_validate($form, &$form_state) {
  $selected_pages = $form_state['values']['video_listing_fieldset']['select_all'];
  $listing = array_filter($form_state['values']['video_listing_fieldset']['videolistings']);
  if (empty($selected_pages[1])) {
    if (empty($listing)) {
      form_set_error('', t('Please select videos from below list.'));
    }
  }
}

/**
 * Submit handler assign job to company form.
 */
function assign_job_company_form_submit($form, &$form_state) {
  global $user;

  if (!empty($_SESSION['search_data']['job_type']) && $_SESSION['search_data']['job_type'] == 2) {
    $assigned = get_already_assigned_videos2($_SESSION['search_data']['job_type'], $_SESSION['search_data']['dest_language']);
    $selected_language = $_SESSION['search_data']['dest_language'];
    $dis_channel = $_SESSION['search_data']['dest_user_listing'];
  }
  else {
    $assigned = get_already_assigned_videos2($_SESSION['search_data']['job_type']);
    $entity_id = db_select('field_data_field_language_code', 'fdflc')
      ->fields('fdflc', array('entity_id'))
      ->condition('field_language_code_value', $_SESSION['search_data']['language'], '=')
      ->execute()->fetchField();
    $selected_language = ($entity_id) ? $entity_id : 32;//50; // 50 for no any language.
    $dis_channel = '';
  }

  $selected_jobtype = $form_state['values']['selected_jobtype'];
  $selected_company_user = $form_state['values']['company_user'];
  $comment = $form_state['values']['comment'];
  $channel = $form_state['values']['user_name'];
  $playlist_name = $form_state['values']['playlist_name'];
  if (!empty($form_state['values']['video_listing_fieldset']['select_all'][1])) {
    $recored = array_diff($form_state['values']['for_all_pages'], $assigned);
  }
  else {
    $recored = array_filter($form_state['values']['video_listing_fieldset']['videolistings']);
    $recored = array_diff($recored, $assigned);
  }

  if (!empty($recored)) {
    // Create node for company_assigned_task.
    $companynode = new stdClass();
    $companynode->type = "company_assigned_task";
    $companynode->language = LANGUAGE_NONE;
    node_object_prepare($companynode);
    $companynode->uid = 1;
    $companynode->field_job_assigned_company[LANGUAGE_NONE][0]['target_id'] = $selected_company_user;
    $companynode->field_dm_admin_reference[LANGUAGE_NONE][0]['target_id'] = $user->uid;
    $companynode->field_cat_language[LANGUAGE_NONE][0]['tid'] = $selected_language;
    $companynode->field_job_type[LANGUAGE_NONE][0]['tid'] = $selected_jobtype;
    $companynode->field_channel[LANGUAGE_NONE][0]['value'] = $channel;
    $companynode->field_playlist[LANGUAGE_NONE][0]['value'] = $playlist_name;
    $companynode->body['und'][0]['value'] = ($comment);
    $companynode->body['und'][0]['format'] = 'full_html';
    $companynode->body['und'][0]['safe_value'] = ($comment);
    $companynode = node_submit($companynode);
    node_save_action($companynode);

    // Send mail to company.
    $user_fields = user_load($selected_company_user);
    $usermail = $user_fields->mail;
    $parms['subject'] = 'New Job Assignment';
    $parms['body'] = "<br/>Hi " . $user_fields->field_first_name[LANGUAGE_NONE]['0']['value'] . "<br/><br/>New Job has been assigned to you by DM admin at dailymotion metadata. <br/><br/>Regards<br/>Daily Motion";
    drupal_mail('forgot_username', 'send_mail', $usermail, language_default(), $parms, variable_get('site_name', ''));

    $count = count($recored);
    $operations = array();
    $limit = $videos_per_batch = 100;
    $offset = 0; $i = 0;
    $reco = array_chunk(array_values($recored), $limit, TRUE);
    while ($offset < $count) {
      $operations[] = array(
        'batch_me',
        array( $reco[$i++],
          $companynode->nid,
          $selected_language,
          $selected_company_user,
          $selected_jobtype,
          $user->uid,
          $comment,
          $channel,
          $playlist_name,
          $dis_channel,
        ),
      );
      $limit = $limit + 100;
      $offset = $offset + 100;
    }
    $batch = array(
      'init_message' => t('Batch process is starting.'),
      'title' => 'Assigning videos in progress',
      'operations' => $operations,
      'finished' => 'batch_me_finished',
      'progress_message' => t($count . ' videos are assigning to ' . $user_fields->field_company_name[LANGUAGE_NONE]['0']['value']),
      'file' => drupal_get_path('module', 'dm_meta_data') . '/include/submit_job_form.admin.inc',
    );
    batch_set($batch);
  }
  else {
    drupal_set_message('0 videos Processed.');
  }
}

/**
 * Custom function to assemble renderable array for block content.
 */
function batch_me($selected_record = array(), $company_nid, $selected_language = NULL, $selected_company_user = NULL, $selected_jobtype = NULL, $dm_admin_id = NULL, $comment = NULL, $channel = NULL, $playlist_name = NULL, $dis_channel = NULL, &$context) {

  foreach ($selected_record as $key => $value) {
    $videonode = new stdClass();
    $videonode->type = "video_management";
    $videonode->title = $value;
    $videonode->language = LANGUAGE_NONE;
    node_object_prepare($videonode);
    $videonode->uid = 1;
    $videonode->field_parent_node_reference[LANGUAGE_NONE][0]['target_id'] = $company_nid;
    $videonode->field_video_id[LANGUAGE_NONE][0]['value'] = $value;
    $videonode->field_video_channel[LANGUAGE_NONE][0]['value'] = $channel;
    $videonode->field_video_playlist_name[LANGUAGE_NONE][0]['value'] = $playlist_name;
    $videonode->field_video_destination_channel[LANGUAGE_NONE][0]['value'] = $dis_channel;
    $videonode = node_submit($videonode);
    node_save_action($videonode);

    $context['results'][] = $videonode->title;
    // Comment : $context['message'] = check_plain($videonode->title); .
  }
}

/**
 * Batch finish.
 */
function batch_me_finished($success, $results, $operations) {
  if ($success) {
    $message = count($results) . ' videos processed.';
  }
  else {
    $error_operation = reset($operations);
    $message = 'An error occurred while processing ' . $error_operation[0] . ' with arguments :' . print_r($error_operation[0], TRUE);
  }
  @$_SESSION['search_data'] = '';
  drupal_set_message($message);
}
