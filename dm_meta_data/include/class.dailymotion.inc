<?php

class DailymotionOwnMethod {
  
  // For create instance of daliymotion.
  protected $dailymotion;
  // For retun data.
  public $returndata = array();
  public $dailymotionlink = 'http://www.dailymotion.com/';

  public function __construct() {
    $this->dailymotion = new Dailymotion();
    $api_key = variable_get('api_key', '');
    $api_secret = variable_get('api_secret', '');
    $api_username = variable_get('api_username', '');
    $api_password = variable_get('api_password', '');
    $this->dailymotion->setGrantType(Dailymotion::GRANT_TYPE_PASSWORD, $api_key, $api_secret, $scope = array(
      'manage_videos'
        ), array(
      'username' => $api_username,
      'password' => $api_password
    ));
  }

  /**
   * Get assosciated chennel list from dailymotion.
   */
  public function getDailymotionChannelList() {
    $resultdata = array();
    try {
      $result = $this->dailymotion->get('/channels', array(
        'fields' => array(
          'id',
          'name'
        )
      ));
      if (isset($result['list']) && !empty($result['list'])) {
        foreach ($result['list'] as $media) {
          $resultdata[$media['id']] = $media['name'];
        }
      }
      return ($resultdata);
    } catch (Exception $e) {
      watchdog('dailymotion', 'Function : getDailymotionChannelList Mesg : @msg', array(
        '@msg' => $e->getMessage()
          ), WATCHDOG_ERROR);
      $resultdata['error'] = $e->getMessage();
      return $resultdata;
    }
  }

  /**
   * get assosciated users list from dailymotion.
   */
  public function getDailymotionUsersList($search_text = null) {
    $resultdata = array();
    try {
      if (!empty($search_text)) {
        $result = $this->dailymotion->get('/users', array(
          'fields' => array('id', 'username'),
          'search' => $search_text,
          'page' => 1,
          'limit' => 40
        ));

        if (isset($result['list']) && !empty($result['list'])) {
          foreach ($result['list'] as $users) {
            $resultdata[$users['username'] . ':' . $users['id']] = $users['username'];
          }
        }
      }

      return ($resultdata);
    } catch (Exception $e) {
      watchdog('dailymotion', 'Function : getDailymotionUsersList Mesg : @msg', array(
        '@msg' => $e->getMessage()
          ), WATCHDOG_ERROR);
      $resultdata['error'] = $e->getMessage();
      return $resultdata;
    }
  }

  /**
   * get assosciated destination channel of original channel (parent user) list from dailymotion.
   */
  public function getDailymotionChildUsersList($parentid) {
    $resultdata = array();
    try {
      if (!empty($parentid)) {
        $result = $this->dailymotion->get('/user/' . $parentid . '/children', array(
          'fields' => array('id', 'username'),
          'page' => 1,
          'limit' => 40
        ));
        if (isset($result['list']) && !empty($result['list'])) {
          foreach ($result['list'] as $users) {
            $resultdata[$users['username'] . ':' . $users['id']] = $users['username'] . ':' . $users['id'];
          }
        }
      }
      return ($resultdata);
    }
    catch (Exception $e) {
      watchdog('dailymotion', 'Function : getDailymotionChildUsersList Msg : @msg', array(
        '@msg' => $e->getMessage()
          ), WATCHDOG_ERROR);
      $resultdata['error'] = $e->getMessage();
      return $resultdata;
    }
  }

  /**
   * Get assosciated users list from dailymotion.
   */
  public function getDailymotionUsersPlaylists($user_id = NULL) {
    $resultdata = array();
    try {
      if (!empty($user_id)) {
        $result = $this->dailymotion->get("/user/$user_id/playlists", array(
          'fields' => array('id', 'name'),
          'page' => 1,
          'limit' => 100
        ));

        if (isset($result['list']) && !empty($result['list'])) {
          $resultdata[] = "-- Fetch videos from all --";
          foreach ($result['list'] as $playlists) {
            $resultdata[$playlists['id']] = $playlists['name'];
          }
        }
      }

      return ($resultdata);
    } catch (Exception $e) {
      watchdog('dailymotion', 'Function : getDailymotionUsersPlaylists Mesg : @msg', array(
        '@msg' => $e->getMessage()
          ), WATCHDOG_ERROR);
      $resultdata['error'] = $e->getMessage();
      return $resultdata;
    }
  }

  /**
   * Get dailymotion video lists.
   */
  public function getDailymotionVideoList($fields = array('id', 'title'), $search_title = array()) {
    $resultdata = array();
    try {
      if (!empty($search_title)) {
        $arr_1 = array('fields' => $fields);
        $arr_2 = $search_title;
        $arra_merge = array_merge($arr_1, $arr_2);
        $resultdata = $this->dailymotion->get('/videos', $arra_merge);
        return ($resultdata);
      }
      return ($resultdata);
    } catch (Exception $e) {
      watchdog('dailymotion', 'Function : getDailymotionVideoList all video Mesg : @msg', array(
        '@msg' => $e->getMessage()
          ), WATCHDOG_ERROR);
      $resultdata['error'] = $e->getMessage();
      return $resultdata;
    }
  }

  /**
   * Get dailymotion video lists.
   */
  public function getDailymotionVideoListByPlaylist($playlist_id = null, $fields = array('id', 'title'), $search_data = array()) {
    $resultdata = array();
    try {
      if (!empty($playlist_id) && !empty($search_data)) {
        $arr_1 = array('fields' => $fields);
        $arr_2 = $search_data;
        $arra_merge = array_merge($arr_1, $arr_2);
        $resultdata = $this->dailymotion->get("/playlist/$playlist_id/videos", $arra_merge);
        return ($resultdata);
      }
      return ($resultdata);
    } catch (Exception $e) {
      watchdog('dailymotion', 'Function : getDailymotionVideoListByPlaylist all video Mesg : @msg', array(
        '@msg' => $e->getMessage()
          ), WATCHDOG_ERROR);
      $resultdata['error'] = $e->getMessage();
      return $resultdata;
    }
  }
  
  /**
   * Get user associated with playlist or video id
   */
  public function getDailyMotionAssociatedUser($urltype, $id) {
    $resultdata = array();
    try {

      $resultdata = $this->dailymotion->get("/$urltype/$id", array(
        'fields' => 'owner.username',
      ));
      return $resultdata;
    } catch (Exception $e) {
      watchdog('dailymotion', 'Function : getDailyMotionAssociatedUser Msg : @msg', array(
        '@msg' => $e->getMessage()
          ), WATCHDOG_ERROR);
      $resultdata['error'] = $e->getMessage();
      return $resultdata;
    }
  }
  
  
  /**
   * Get dailymotion video lists.
   */
  public function getDailymotionVideoListByUserID($user_id = null, $fields = array('id', 'title'), $search_data = array()) {
    $resultdata = array();
    try {
      if (!empty($user_id) && !empty($search_data)) {
        $arr_1 = array('fields' => $fields);
        $arr_2 = $search_data;
        $arra_merge = array_merge($arr_1, $arr_2);
        $resultdata = $this->dailymotion->get("/user/$user_id/videos", $arra_merge);
        return ($resultdata);
      }
      return ($resultdata);
    } catch (Exception $e) {
      watchdog('dailymotion', 'Function : getDailymotionVideoListByUserID all video Mesg : @msg', array(
        '@msg' => $e->getMessage()
          ), WATCHDOG_ERROR);
      $resultdata['error'] = $e->getMessage();
      return $resultdata;
    }
  }

  /**
   * Delete dailymotion video by its video id.
   */
  public function deleteDailyMotionVideo($video_id = null) {
    try {
      $result = $this->dailymotion->delete("/video/$video_id");
      watchdog('dailymotion', 'Dailymotion video deleted successfully by @user and video ID  @video_id', array(
        '@user' => @$GLOBALS['user']->name,
        '@video_id' => $video_id
          ), WATCHDOG_INFO);
    } catch (Exception $e) {
      watchdog('dailymotion', 'Function : deleteDailyMotionVideo Mesg : @msg', array(
        '@msg' => $e->getMessage()
          ), WATCHDOG_ERROR);
      return $resultdata['error'] = $e->getMessage();
    }
  }

  /**
   * Get dailymotion video details.
   */
  public function getDailyMotionVideoDetail($videoId = null, $fields = array('id', 'title', 'embed_url', 'channel', 'thumbnail_url', 'description', 'tags', 'private', 'type')) {
    $resultdata = array();
    try {

      $resultdata = $this->dailymotion->get("/video/$videoId", array(
        'fields' => $fields
      ));
      return $resultdata;
    } catch (Exception $e) {
      watchdog('dailymotion', 'Function : getDailyMotionVideoDetail Mesg : @msg', array(
        '@msg' => $e->getMessage()
          ), WATCHDOG_ERROR);
      $resultdata['error'] = $e->getMessage();
      return $resultdata;
    }
  }

  /**
   * Update dailymotion video data.
   */
  public function visitorCountry() {
    $result = "";
    if (@filter_var($_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP)) {
      $ip = $_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (@filter_var($_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP)) {
      $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else {
      $ip = $_SERVER['REMOTE_ADDR'];
    }
    $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
    if ($ip_data && $ip_data->geoplugin_countryCode != NULL) {
      $result = $ip_data->geoplugin_countryCode;
    }
    return !empty($result) ? $result : 'us';
  }

  /**
   * Update dailymotion video data.
   */
  public function updateDailymotionVideoData($videoId = null, $data = array()) {
    try {
      $result = $this->dailymotion->post("/video/$videoId", $data);
      watchdog('dailymotion', 'Dailymotion video data updated successfully by @user and video ID  @video_id', array(
        '@user' => @$GLOBALS['user']->name,
        '@video_id' => $videoId
          ), WATCHDOG_INFO);
    } catch (Exception $e) {
      watchdog('dailymotion', 'Function : updateDailymotionVideoData Mesg : @msg', array(
        '@msg' => $e->getMessage()
          ), WATCHDOG_ERROR);
      return $resultdata['error'] = $e->getMessage();
    }
  }

  /**
   * Update dailymotion video data.
   */
  public function getDailymotionConnectedInformation() {
    $resultdata = array();
    $videoresult = $this->dailymotion->get('/me/videos', array(
      'fields' => array(
        'id',
        'title',
        'created_time'
      ),
      'page' => 1,
      'limit' => 10
    ));
    $userresult = $this->dailymotion->get('/user/me', array(
      'fields' => array(
        'id',
        'screenname',
        'type',
        'avatar_120_url'
      )
    ));
    $resultdata['screenname'] = !empty($userresult['screenname']) ? $userresult['screenname'] : 'screenname not found';
    $resultdata['total_record'] = !empty($videoresult['total']) ? $videoresult['total'] : 0;
    $resultdata['user_photo'] = !empty($userresult['avatar_120_url']) ? $userresult['avatar_120_url'] : '';
    $resultdata['last_uploaded'] = !empty($videoresult['list'][0]['created_time']) ? date('M d, Y', $videoresult['list'][0]['created_time']) : 'Not Found';
    return $resultdata;
  }

  /**
   * Check user is official.
   */
  public function getUserType() {
    $userresult = $this->dailymotion->get('/user/me', array(
      'fields' => array(
        'type'
      )
    ));
    if (!empty($userresult['type']) && ($userresult['type'] == 'official')) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Update dailymotion video data.
   */
  public function getDMAuthorizationUrl($display = 'popup') {
    return $this->dailymotion->getAuthorizationUrl($display);
  }

  /**
   * Upload dailymotion video.
   */
  public function uploadVideoOnDailymotion($testVideoFile = null, $video_title = 'Dailymotion Video', $channel = null) {
    $url = $this->dailymotion->uploadFile($testVideoFile);
    if (!empty($channel)) {
      $media = $this->dailymotion->post('/videos', array(
        'url' => $url,
        'title' => $video_title,
        'published' => TRUE,
        'channel' => $channel
      ));
    }
    else {
      $media = $this->dailymotion->post('/videos', array(
        'url' => $url,
        'title' => $video_title,
        'published' => true
      ));
    }
    watchdog('dailymotion', 'Dailymotion video upload successfully by @user and video ID  @video_id', array(
      '@user' => @$GLOBALS['user']->name,
      '@video_id' => $media['id']
        ), WATCHDOG_INFO);
    return $media;
  }

  /**
   * Get access token from dailymotion.
   */
  private function dailymotionAccessToken() {
    try {
      // $access_token = $this->dailymotion->getAccessToken();
      $access_token = $this->dailymotion->oauthTokenRequest(array(
        'grant_type' => 'password',
        'client_id' => variable_get('api_key', ''),
        'client_secret' => variable_get('api_secret', ''),
        'username' => variable_get('api_username', ''),
        'password' => variable_get('api_password', ''),
      ));
      return $access_token;
    }
    catch (Exception $e) {
      watchdog('dailymotion', 'Function : dailymotionAccessToken Mesg : @msg', array(
        '@msg' => $e->getMessage()
          ), WATCHDOG_ERROR);
      return $resultdata['error'] = $e->getMessage();
    }
  }

  /**
   * Drupal_http_request.
   */
  private function DailymotionProxyRequest($url, $data) {
    $headers[] = 'Expect:';
    $data = http_build_query($data);
    $ch = curl_init();
    curl_setopt_array($ch, array(
      CURLOPT_PROXY => NULL,
      CURLOPT_HEADER => FALSE,
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_URL => $url,
      CURLOPT_HTTPHEADER => $headers,
      CURLOPT_POSTFIELDS => $data,
    ));
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
  }

  /**
   * Add strongtags in video on dailymotion .
   */
  public function addStrongtagsVideoOnDailymotion($vid, $provider, $lang, $mid, $channel) {
    try {
      $media = $this->dailymotionAccessToken();
      $data = array(
        'access_token' => $media['access_token'],
        'mid' => $mid,
        'channel' => $channel,
        'language' => $lang,
        'provider' => $provider,
        'video_id' => $vid,
      );
      $media2 = $this->DailymotionProxyRequest('https://dm-saakai-adapter.herokuapp.com/strongtag', $data);
      return $media2;
    }
    catch (Exception $e) {
      watchdog('test', 'Function : addStrongtagsVideoOnDailymotion Msg : @msg', array(
        '@msg' => $e->getMessage()
          ), WATCHDOG_ERROR);
      return $resultdata['error'] = $e->getMessage();
    }
  }

  /**
   * Duplicate Video for dailymotion.
   */
  public function VideoDuplicateOnDailymotion($from_channel, $to_channel, $url, $description, $title, $language) {
    try {
      $media = $this->dailymotionAccessToken();
      $data = array(
        'from_channel' => $from_channel,
        'to_channel' => $to_channel,
        'url' => $url,
        'access_token' => $media['access_token'],
        'description' => $description,
        'title' => $title,
        'language' => $language,
      );
      $media2 = $this->DailymotionProxyRequest('https://dm-saakai-adapter.herokuapp.com/duplicate', $data);
      return $media2;
    }
    catch (Exception $e) {
      watchdog('test', 'Function : testVideoDuplicateApi Msg : @msg', array(
        '@msg' => $e->getMessage()
          ), WATCHDOG_ERROR);
      return $resultdata['error'] = $e->getMessage();
    }
  }

  /**
   * Check if a given user object is connected.
   */
  public function checkParentUserofDailymotion($child_id, $parent_id) {
    $resultdata = array();
    try {
      if (!empty($parent_id)) {
        $result = $this->dailymotion->get('/user/' . $child_id . '/parents/' . $parent_id, array(
          'fields' => array('id', 'username'),
        ));
        if (isset($result['list']) && !empty($result['list'])) {
          foreach ($result['list'] as $users) {
            $resultdata[$users['username'] . ':' . $users['id']] = $users['username'];
          }
        }
      }
      return ($resultdata);
    }
    catch (Exception $e) {
      watchdog('dailymotion', 'Function : checkParentUserofDailymotion Msg : @msg', array(
        '@msg' => $e->getMessage()
          ), WATCHDOG_ERROR);
      $resultdata['error'] = $e->getMessage();
      return $resultdata;
    }
  }
}
