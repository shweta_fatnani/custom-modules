jQuery(document).ready(function() {

    //var drupal_arg = Drupal.settings.drupal_arguments;
    jQuery('#display_by_default_strong_tag_page').change(function(){
        var aBasepath = location.protocol + "//" + location.host+Drupal.settings.basePath;
        var match = splitCurrentUrl('jobs-status');
        if(match[1]) {
            var redirect = aBasepath+'translate-jobs-status'+match[1]
        } else {
            var redirect = aBasepath+'translate-jobs-status'
        }
        window.location.replace(redirect);
    });
    jQuery('#display_by_default_translation_page').change(function(){
        var aBasepath = location.protocol + "//" + location.host+Drupal.settings.basePath;
        var match = splitCurrentUrl('translate-jobs-status');
        if(match[1]) {
            var redirect = aBasepath+'jobs-status'+match[1]
        } else {
            var redirect = aBasepath+'jobs-status'
        }
        window.location.replace(redirect);
    });
});

function splitCurrentUrl(splitText) {
    var url = jQuery(location).attr('href');
    var pieces = url.split(splitText);
    return pieces;
}