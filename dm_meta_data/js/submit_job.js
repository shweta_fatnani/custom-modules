(function ($) {
    Drupal.behaviors.submit_job_config_form = {
        attach: function (context, settings) {
            
            $('.select_all').hide();
            $('select[id^="edit-video-listing-fieldset-select-all"]').children('option[value="2"]').hide();
            $('select[id^="edit-video-listing-fieldset-select-all"]').children('option[value="0"]').attr('selected', false);
            $('select[id^="edit-video-listing-fieldset-select-all"]').children('option[value="1"]').attr('selected', false);
            $('select[id^="edit-video-listing-fieldset-select-all"]').children('option[value="2"]').attr('selected', false);
            $('.open-select-all [id^="edit-video-listing-fieldset-radio-click"]').attr('checked', false);
            
            $('.open-select-all [id^="edit-video-listing-fieldset-radio-click"]').toggle(function() {
                $(this).attr('checked', false);
                $('.select_all').show();
                $('body').click(function() {
                    $('.select_all').hide();
                });
                
            },function(){
                $(this).attr('checked', false);
                $('.select_all').show();
                $('body').click(function() {
                    $('.select_all').hide();
                });
            });
            
            // for page selection.
            $('.select_all').change(function() {
                if($('.select_all option:selected').text() == 'Remove all') {
                    $("#video-listing-wrapper input[type='checkbox']").each(function () {
                        if (!$(this).attr('disabled')) {
                            $(this).attr('checked', false);
                        }
                    });
                    $('select[id^="edit-video-listing-fieldset-select-all"]').children('option[value="2"]').hide();
                    $('.panel-body .open-select-all label').text("Check all");
                    $('.open-select-all').attr('checked', false);
                    $('#video-listing-wrapper #video-listing-wrapper .panel-body button').attr('disabled',false);
                }
                if($('.select_all option:selected').text() == 'Only this page') {
                    $("#video-listing-wrapper input[type='checkbox']").each(function () {
                        if (!$(this).attr('checked') && !$(this).attr('disabled')) {
                            $(this).attr('checked', 'checked');
                        }
                    });
                    $('select[id^="edit-video-listing-fieldset-select-all"]').children('option[value="2"]').show();
                    $('.panel-body .open-select-all label').text("Only this page");
                    $('.open-select-all').attr('checked', true);
                    $('#video-listing-wrapper #video-listing-wrapper .panel-body button').attr('disabled',false);
                }
                if($('.select_all option:selected').text() == 'All pages') {
                    $("#video-listing-wrapper input[type='checkbox']").each(function () {
                        if (!$(this).attr('checked') && !$(this).attr('disabled')) {
                            $(this).attr('checked', 'checked');
                        }
                    });
                    $('select[id^="edit-video-listing-fieldset-select-all"]').children('option[value="2"]').show();
                    $('.panel-body .open-select-all label').text("Selected all pages.");
                    $('.open-select-all').attr('checked', true);
                    $('#video-listing-wrapper #video-listing-wrapper .panel-body button').attr('disabled','disabled');
                }
                
                $('.select_all').hide();
                $('.open-select-all').show();
            });
        }
    };
})(jQuery);