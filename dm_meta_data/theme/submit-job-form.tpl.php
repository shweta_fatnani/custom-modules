<?php
if (empty($_SESSION['created_for_search_data'])) {
    $_SESSION['search_data'] = $search_data = '';
    $_SESSION['created_for_search_data'] = time();
  }
  elseif ((time() - $_SESSION['created_for_search_data']) < 3600) {
    // Update creation time.
    $_SESSION['created_for_search_data'] = time();
  }
  elseif ((time() - $_SESSION['created_for_search_data']) > 3600) {
    $_SESSION['search_data'] = $search_data = '';
    $_SESSION['created_for_search_data'] = '';
  }
?>
<div class="main_content">
   <div class="submit_job_config_form"><?php $submit_job_config_form = drupal_get_form('submit_job_config_form'); print drupal_render($submit_job_config_form); ?></div>
   <?php if (!empty($search_data)) : ?>
   <div class="assign_job_config_form"><?php $assign_job_company_form = drupal_get_form('assign_job_company_form', $search_data); print drupal_render($assign_job_company_form); ?></div>
   <?php endif; ?>
</div>